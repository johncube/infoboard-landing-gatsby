import { StaticImage } from 'gatsby-plugin-image'
import React from 'react'
import ListItem from '../list_item/list_item'

const PostList = ({ posts }) => {
    return (
        <div className="min-h-screen mt-44 container mx-auto px-4 lg:px-32">
            <div className="hidden xl:block w-56 h-56 absolute -right-28 top-1/3">
                <StaticImage src="../../../images/semicircle.png" alt="dots" />
            </div>
            <div className="hidden xl:block w-56 h-56 absolute -left-24 top-3/4">
                <StaticImage src="../../../images/dots.png" alt="dots" />
            </div>
            <div className="w-full grid grid-cols-1 md:grid-cols-2 gap-8 pb-12">
                {posts.map(p => <ListItem key={p.node.id} id={p.node.id} slug={p.node.slug} title={p.node.title} text={p.node.text} thumbnail={p.node.thumbnail} />)}
            </div>
        </div>
    )
}

export default PostList