import { Link } from 'gatsby-plugin-react-i18next'
import React, { useState } from 'react'
import sample from '../../images/device_sample.png'
import Dialog from './dialog/dialog'


const ImageSwitch = ({ t }) => {
    const [active, setActive] = useState(0)

    return (
        <div className="bg-gray-200">
            <div className="lg:container mx-auto px-0 md:px-2 2xl:px-44 mt-32 pb-16 pt-8">
                <p className="text-base md:text-lg text-gray-600 text-center px-4 sm:px-20 lg:px-40">
                    Komunikuje to aplikacja z informacjami dla pracowników Twojej firmy. Działa przez przeglądarkę, więc jest dostępna dla każdego pracownika bez względu, czy ma firmowy profil, adres email lub konto.
                    Możesz dowolnie kształtować jej zawartość aby dostarczyć tym, którzy budują wartość Twojej firmy, najważniejsze informacje oraz otrzymać od nich informacje zwrotną.
                    <span className="text-blue-400 mt-6 text-3xl font-bold text-center block">Co otrzymasz w standardowym pakiecie:</span>
                </p>
                <div className="grid lg:grid-cols-2 mt-10">
                    <div className="px-4">
                        <img src={sample} alt="sample" className="mx-auto" />
                    </div>
                    <div className="px-4 lg:px-0">
                        {t.map((t, i) => {
                            return <Dialog key={t.heading} active={active} setActive={setActive} index={i} heading={t.heading} list={t.list} />
                        })}
                        {/* <Dialog active={active} setActive={setActive} index={0} />
                        <Dialog active={active} setActive={setActive} index={1} />
                        <Dialog active={active} setActive={setActive} index={2} />
                        <Dialog active={active} setActive={setActive} index={3} /> */}
                    </div>
                </div>
                <div className="flex justify-center">
                    <Link to="/solutions" className="inline-block text-white bg-blue-400 hover:bg-blue-500 duration-200 text-md sm:text-lg md:text-xl lg:text-2xl px-8 pt-3.5 pb-3 rounded-full mt-12">Komunikuje dla Twojej branży</Link>
                </div>
            </div>
        </div>
    )
}

export default ImageSwitch