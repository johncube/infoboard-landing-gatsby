import * as React from "react"
import { I18nextContext, useTranslation } from 'gatsby-plugin-react-i18next';
import { graphql } from 'gatsby'
import Hero from "../components/hero/hero";
import About from "../components/about/about";
import Information from "../components/information/information";
import Communication from "../components/communication/communication";
import Involvement from "../components/involvement/involvement";
import Devices from "../components/devices/devices";
import Contact from "../components/contact/contact";
import Accordion from "../components/accordion/accordion";
import Footer from "../components/footer/footer";
import Loader from "../components/loader/loader";
import Nav from "../components/navbar/navbar";
// import Prices from '../components/prices/prices'

import '../styles/index.sass'
import Impact from "../components/impact/impact";
import ImageSwitch from "../components/image_switch/image_switch";
import Seo from "../components/SEO/seo";

let intervalTimer = 10000;
// markup
const IndexPage = () => {
  const [loading,] = React.useState(false)
  const [slide, setSlide] = React.useState(1)
  const { t } = useTranslation('translation');
  const context = React.useContext(I18nextContext)

  React.useEffect(() => {
    let interval = setTimeout(() => {
      if (slide + 1 > 3) setSlide(1)
      else setSlide(slide + 1)
    }, intervalTimer)

    return () => {
      clearInterval(interval);
    }
  }, [slide])

  return (
    <main >
      <Seo title={t('title')} lang={context.language} />
      {loading && <Loader />}
      <Nav />
      <Hero t={t('hero', { returnObjects: true })} />
      <About slide={slide} changeSlide={setSlide} t={t('about', { returnObjects: true })} />
      <div className="w-full relative">
        <div className={`container w-full mx-auto px-0 2xl:px-80 relative h-100 lg:h-80 mb-20 sm:mb-24 mt-4 sm:mt-20`}>
          <Information slide={slide} t={t('information', { returnObjects: true })} />
          <Communication slide={slide} t={t('communication', { returnObjects: true })} />
          <Involvement slide={slide} t={t('involvement', { returnObjects: true })} />
        </div>
      </div>
      <ImageSwitch t={t('features', { returnObjects: true })} />
      <Devices t={t('devices', { returnObjects: true })} />
      <Impact />
      {/* <Prices t={t('price', { returnObjects: true })} /> */}
      <Contact t={t('contact', { returnObjects: true })} privacy={t('privacy', { returnObjects: true })} />
      <Accordion t={t('accordion', { returnObjects: true })} />
      <Footer />
    </main>
  )
}

export default IndexPage


export const query = graphql`
  query($language: String!) {
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`;