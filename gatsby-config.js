require("dotenv").config({
  path: `.env`,
});

module.exports = {
  siteMetadata: {
    siteUrl: "https://www.komunikuje.pl",
    title: "komunikuje",
  },
  flags: {
    DEV_SSR: false
  },
  plugins: [
    "gatsby-plugin-styled-components",
    "gatsby-plugin-image",
    'gatsby-plugin-postcss',
    {
      resolve: "gatsby-plugin-google-analytics",
      options: {
        trackingId: "G-LWRXX4VBNY",
      },
    },
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/locales`,
        name: `locale`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `./src/files`,
        name: `files`
      }
    },
    {
      resolve: `gatsby-plugin-react-i18next`,
      options: {
        localeJsonSourceName: `locale`,
        languages: [`en`, `pl`],
        generateDefaultLanguagePage: true,
        defaultLanguage: `pl`,
        redirect: true,
        siteUrl: `https://example.com/`,
        i18nextOptions: {
          interpolation: {
            escapeValue: false
          },
          keySeparator: false,
          nsSeparator: false,
          returnObjects: true
        }
      }
    },
    `gatsby-plugin-sass`,
    {
      resolve: "gatsby-source-strapi",
      options: {
        apiURL: process.env.API_URL || "http://localhost:1337",
        collectionTypes: ["articles"],
        queryLimit: 1000,
      },
    },
  ],
};
