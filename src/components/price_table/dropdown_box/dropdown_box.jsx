import React from 'react'

const DropdownBox = ({ background, border, id, selected, setOpen, t, option, text }) => {
    return (
        <div className={`w-full py-5 ${background} rounded-xl text-white block sm:hidden mt-5`}>
            <div>
                <h1 className="text-3xl font-bold">Basic</h1>
                <h2 className="text-3xl">390 zł<span className="text-xl ml-2">/ miesiąc*</span></h2>
                <button className={`text-2xl bg-white ${text} hover:bg-gray-100 duration-200 px-8 pt-2 pb-2 rounded-full my-2`}>Zainteresowany</button>
            </div>
            <button className="mt-3 cursor-pointer" onKeyDown={() => setOpen(selected === id ? -1 : id)} onClick={() => setOpen(selected === id ? -1 : id)}>{selected === id ? 'Hide features' : 'Show features'}</button>
            <div className={`transition-all duration-500 overflow-hidden mt-3`} style={{ maxHeight: selected === id ? '2000px' : '0px' }}>
                {t.rows.map(r => {
                    return (
                        <div className={`mt-1 border-b ${border} px-5`}>
                            <p className="text-xs text-gray-200">{r.text}</p>
                            <p className="font-bold">{r[option]}</p>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default DropdownBox