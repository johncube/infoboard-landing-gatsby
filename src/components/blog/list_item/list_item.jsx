// import { GatsbyImage } from 'gatsby-plugin-image'
import { Link } from 'gatsby-plugin-react-i18next'
import React from 'react'

const ListItem = ({ title, thumbnail, text, id, slug }) => {
    return (
        <Link to={`/article/${slug}`}>
            <div className="flex flex-col xl:flex-row cursor-pointer hover:bg-gray-100">
                <img className="w-full xl:w-60 h-52 object-cover rounded-xl" width="100%" height="100%" src={thumbnail.url} alt="thumbnail" />
                {/* <GatsbyImage image={'https://komunikuje-blog.herokuapp.com' + thumbnail.url} alt="Thumbnail" imgClassName="w-full h-full" /> */}
                <div className="px-4 flex flex-col justify-around">
                    <h2 className="text-2xl font-bold text-blue-400">{title}</h2>
                    <p>
                        {text.slice(0, 150)}
                    </p>
                </div>
            </div>
        </Link>
    )
}

export default ListItem