import * as React from "react"
import Seo from '../components/SEO/seo'
import Footer from "../components/footer/footer"
import Navbar from "../components/navbar/navbar"
import Solutions from "../components/solutions/solutions"

import '../styles/index.sass'
import { I18nextContext } from "gatsby-plugin-react-i18next"

const SolutionsPage = () => {
    const context = React.useContext(I18nextContext)

    return (
        <main >
            <Seo title={'Rozwiązania'} lang={context.language} />
            <Navbar />
            <Solutions />
            <Footer />
        </main>
    )
}

export default SolutionsPage