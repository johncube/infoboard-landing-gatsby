import React, { useState } from "react"
import AccordionItem from "./accordionItem/accordionItem"

const Accordion = ({ refe, t }) => {

    const [current, setCurrent] = useState(-1)

    return (
        <div className="container mx-auto px-2 sm:px-4 xl:px-32 2xl:px-48 my-12" id="about" ref={refe}>
            <h1 className="text-3xl sm:text-4xl lg:text-5xl pr-4 sm:px-0 pl-4 mb-16 font-bold text-gray-700">
                {t.title}
            </h1>
            <AccordionItem heading={t.heading1} text={t.text1} index={0} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading2} text={t.text2} index={1} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading3} text={t.text3} index={2} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading4} text={t.text4} index={3} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading5} text={t.text5} index={4} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading6} text={t.text6} table t={t} index={5} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading7} text={t.text7} index={6} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading8} text={t.text8} index={7} current={current} setCurrent={setCurrent} />
            <AccordionItem heading={t.heading9} text={t.text9} last index={8} current={current} setCurrent={setCurrent} />
        </div>
    )
}

export default Accordion