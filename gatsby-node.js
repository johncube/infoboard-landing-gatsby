const path = require('path')

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(
    `
      {
        articles: allStrapiArticles {
          edges {
            node {
              slug
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create blog articles pages.
  const articles = result.data.articles.edges

  articles.forEach((article, index) => {
    createPage({
      path: `/article/${article.node.slug}`,
      component: require.resolve('./src/components/blog/detail/detail.jsx'),
      context: {
        slug: article.node.slug
      }
    })
    createPage({
      path: `/pl/article/${article.node.slug}`,
      component: require.resolve('./src/components/blog/detail/detail.jsx'),
      context: {
        slug: article.node.slug
      }
    })
    createPage({
      path: `/en/article/${article.node.slug}`,
      component: require.resolve('./src/components/blog/detail/detail.jsx'),
      context: {
        slug: article.node.slug
      }
    })
  })
}