import React from 'react'
import styled from 'styled-components'

const Dialog = ({ active, setActive, index, heading, list }) => {
    return (
        <div className={`${active !== index && 'lg:pl-10'}`}>
            <button className={`text-left duration-200 w-full p-4 text-xs text-gray-600 ${active === index ? 'bg-blue-300' : 'bg-gray-100'} rounded-xl relative mb-2 border-2 border-blue-300 cursor-pointer`} onClick={() => setActive(index)}>
                {active === index && <Triangle className="absolute right-full top-1/2 transform -translate-y-1/2 hidden lg:block"></Triangle>}
                {heading}
                <ul className="list-disc pl-4">
                    {list.map(l => <li key={l}>{l}</li>)}
                </ul>
            </button>
        </div>

    )
}

export default Dialog

const Triangle = styled.div`
  width: 0; 
  height: 0; 
  border-top: 10px solid transparent;
  border-bottom: 10px solid transparent; 
  border-right:20px solid rgb(136, 216, 241);; 
`