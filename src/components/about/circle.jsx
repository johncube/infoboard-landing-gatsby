import { StaticImage } from 'gatsby-plugin-image'
import React from 'react'

const Circle = ({ img, delay, text, active, changeSlide, slide, imgclass }) => {

    return (
        <div className="col-span-2 flex lg:block justify-center flex-col items-center"
            data-aos="fade-up"
            data-aos-anchor-placement="center-bottom"
            data-aos-delay={delay}
            data-aos-offset="50">
            <button className={`rounded-full ${active ? 'bg-blue-400 w-20 h-20 sm:w-32 sm:h-32 md:w-48 md:h-48 lg:w-64 lg:h-64' : 'bg-blue-300 w-20 h-20 sm:w-28 sm:h-28 md:w-44 md:h-44 lg:w-56 lg:h-56'} duration-1000 mx-auto flex justify-center items-center cursor-pointer`} onClick={() => changeSlide(slide)}>
                <div className={`${active ? 'h-14 w-14 sm:w-18 sm:h-18 md:w-28 md:h-28 lg:w-36 lg:h-36' : 'w-14 h-14 sm:h-22 sm:w-22 md:h-24 md:w-24 lg:h-32 lg:w-32'} ${imgclass}`}>
                    {img === 'information' && <StaticImage src='../../images/hand.png' alt={text} imgClassName="w-full h-full" />}
                    {img === 'communication' && <StaticImage src='../../images/megaphone.png' alt={text} imgClassName="w-full h-full" />}
                    {img === 'engagement' && <StaticImage src='../../images/hands.png' alt={text} imgClassName="w-full h-full" />}
                </div>

            </button>
            <p className={`text-center text-sm md:text-2xl mt-4 ${active ? 'text-blue-400' : 'text-blue-300'} duration-1000 font-medium`}>{text}</p>
        </div>
    )
}

export default Circle