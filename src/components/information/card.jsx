import React from 'react'

import expand from '../../images/full-screen.svg'
import play from '../../images/play.svg'
import next from '../../images/next.svg'
import previous from '../../images/previous.svg'

const Card = ({title, img, cardClass, delay}) => {
    return (
        <div className={`card p-1 rounded shadow-md absolute ${cardClass} bg-white`}
            data-aos="fade-up"
            data-aos-anchor-placement="center-bottom"
            data-aos-offset="50"
            data-aos-delay={delay}>
            <p className="text-sm sm:text-md font-normal text-gray-600 pl-2">{title}</p>
            <div className="flex mt-4">
                <img src={img} className="w-22 h-20 mr-4" alt="card" />
                <p className="text-xs sm:text-sm font-light text-gray-500 pr-3 card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Architecto iusto, temporibus saepe velit aliquid voluptates nam dignissimos voluptatem aut doloribus, rerum rem nemo veritatis ab, fuga earum?</p>
            </div>
            <div className="flex justify-between mt-4 px-2">
                <div className="flex">
                    <img src={expand} className="w-4 mr-1" alt="expand" />
                    <img src={play} className="w-5" alt="play" />
                </div>
                <div className="flex">
                    <img src={previous} className="w-5 mr-1" alt="prev" />
                    <img src={next} className="w-5" alt="next" />
                </div>
            </div>
        </div>
    )
}

export default Card