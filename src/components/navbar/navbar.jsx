import React, { useEffect, useRef, useState } from 'react'
import { I18nextContext, Link, useI18next } from 'gatsby-plugin-react-i18next';
import logo from '../../images/logo.png'
import polish from '../../images/polish_flag.png'
import english from '../../images/uk_flag.png'
import styled from 'styled-components';

const Navbar = () => {
    const [menu, toggleMenu] = useState(false)

    const ref = useRef(null)
    const logoRef = useRef(null)
    const { changeLanguage } = useI18next();
    const context = React.useContext(I18nextContext)
    // const { i18n } = useTranslation()

    useEffect(() => {
        window.addEventListener('scroll', function () {
            if (ref && ref.current) {
                let distance = ref.current.getBoundingClientRect().top + window.scrollY;
                if (window.innerWidth > 640) {
                    if (distance > 50) {
                        ref.current.classList.remove('py-4', 'sm:bg-transparent')
                        ref.current.classList.add('py-2')
                        logoRef.current.classList.remove('h-18', 'sm:h-16', '2xl:h-24')
                        logoRef.current.classList.add('h-10', 'sm:h-12', '2xl:h-14')
                    }
                    else {
                        ref.current.classList.add('py-4', 'sm:bg-transparent')
                        ref.current.classList.remove('py-2')
                        logoRef.current.classList.add('h-18', 'sm:h-16', '2xl:h-24')
                        logoRef.current.classList.remove('h-10', 'sm:h-12', '2xl:h-14')
                    }
                }
            }
        })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ref])

    return (
        <div className={`w-full fixed top-0 left-0 z-40 transition-all duration-200 py-3 sm:px-2 xl:px-20 bg-white sm:bg-transparent`} ref={ref}>
            <div className="relative flex justify-between items-center px-4 ">
                <Link to="/">
                    <img src={logo} width="100%" height="100%" ref={logoRef} className={`h-12 sm:h-16 2xl:h-24 transition-all duration-200 `} alt="logo" />
                </Link>
                <div className="flex items-center justify-between w-18 h-full lang-container">
                    <Dropdown open={menu} className="text-lg mr-5 absolute sm:relative top-full left-0 flex flex-col sm:block bg-white sm:bg-transparent w-full">
                        {/* <CustomLink to="/" className="mr-4 uppercase text-lg sm:text-sm lg:text-base pl-4 sm:pl-0 py-1 cursor-pointer">Strona główna</CustomLink> */}
                        <CustomLink to="/solutions" className="mr-4 uppercase text-lg sm:text-sm lg:text-base pl-4 sm:pl-0 py-1 cursor-pointer">Rozwiązania</CustomLink>
                        <CustomLink to="/price" className="mr-4 uppercase text-lg sm:text-sm lg:text-base pl-4 sm:pl-0 py-1 cursor-pointer">Cennik</CustomLink>
                        <CustomLink to="/blog" className="mr-4 uppercase text-lg sm:text-sm lg:text-base pl-4 sm:pl-0 py-1 cursor-pointer">Blog</CustomLink>
                        <CustomLink to="/demo" className="mr-4 uppercase text-lg sm:text-sm lg:text-base pl-4 sm:pl-0 py-1 cursor-pointer">Demo</CustomLink>
                    </Dropdown>
                    <img src={polish} width="100%" height="100%" className={`h-4 cursor-pointer ${context.language === 'en' && 'filter-grayscale'} lang-icon mr-1`} alt="pl" onClick={() => changeLanguage('pl')} onKeyDown={() => changeLanguage('pl')} />
                    <img src={english} width="100%" height="100%" className={`h-4 cursor-pointer ${context.language === 'pl' && 'filter-grayscale'} lang-icon`} alt="en" onClick={() => changeLanguage('en')} onKeyDown={() => changeLanguage('en')} />
                    <div className="mt-2 ml-4">
                        <button aria-label="hamburger" onClick={() => toggleMenu(!menu)} className={`hamburger hamburger--squeeze  ${menu && 'is-active'} inline-block sm:hidden`}>
                            <div className="hamburger-box">
                                <div className="hamburger-inner"></div>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar

const Dropdown = styled.div`
    @media (max-width: 640px){
        height: ${props => props.open ? '100vh' : '0'};
        transition: all .4s;
        overflow: hidden;
        background-color: rgba(255,255,255,1);
        backdrop-filter: blur(15px);
        padding-bottom: ${props => props.open ? '10rem' : '0'};;
    }
`

const CustomLink = styled(Link)`
    &::after{
        content: "|";
        margin-left: 1rem;
        position: relative;
        top: -.1rem
    }

    &:last-child{
        &::after{display: none;}
    }

    @media (max-width: 640px){
        font-size: 11vw;
        line-height: 3rem;
        font-weight: 700;
        margin-top: 1rem;
        &::after{
            display: none;
        }

        &:first-child{
            margin-top: 2rem;
        }
    }

    &.active{
        color: red;
    }
`