import React from 'react'
import { AutoHeightTransition } from '../../../styles/styled-components/styled_components'

import testimg from '../../../images/involvement.png'

const SolutionDetail = ({ open }) => {
    return (
        <AutoHeightTransition open={open} className="px-2">
            <p className="text-base sm:text-lg lg:text-xl font-semibold text-gray-600 text-center mt-8">
                Komunikacja w zakładach produkcyjnych to Komunikacja w zakładach produkcyjnych to Komunikacja w zakładach produkcyjnych to Komunikacja w zakładach produkcyjnych to Komunikacja w zakładach produkcyjnych
            </p>
            <h2 className="text-2xl sm:text-3xl lg:text-4xl font-bold mt-10 text-blue-400 text-center">
                Komunikacja w zakladach produkcyjnych to:
            </h2>
            <div className="flex flex-col md:flex-row mt-12">
                <div className="w-full md:w-2/5">
                    <img src={testimg} width="100%" height="100%" alt="worker" className="object-cover rounded-3xl h-full" />
                </div>
                <div className="md:pl-20 w-full md:w-3/5">
                    <h2 className="text-2xl sm:text-3xl lg:text-4xl font-bold mt-3 text-blue-400 text-left">
                        Lorem ipsum dolor sit
                    </h2>
                    <p className="text-base sm:text-lg text-gray-600 mt-4">
                        amet, consectetur adipiscing elit. Etiam sollicitudin eu  ibero sit amet posuere. Aenean malesuada lacus eget bibendum euismod. Vestibulum hendrerit eros vitae dapibus finibus. Suspendisse potenti. Suspendisse imperdiet lobortis purus, eu eleifend leo imperdiet id.
                        <br /> <br />
                        amet, consectetur adipiscing elit. Etiam sollicitudin eu  ibero sit amet posuere. Aenean malesuada lacus eget bibendum euismod. Vestibulum hendrerit eros vitae dapibus finibus. Suspendisse potenti. Suspendisse imperdiet lobortis purus, eu eleifend leo imperdiet id.
                    </p>
                </div>
            </div>
            <div className="flex flex-col-reverse md:flex-row mt-12">
                <div className="md:pr-20 w-full md:w-3/5">
                    <h2 className="text-2xl sm:text-3xl lg:text-4xl font-bold mt-3 text-blue-400 text-left">
                        Lorem ipsum dolor sit
                    </h2>
                    <p className="text-base sm:text-lg text-gray-600 mt-4">
                        amet, consectetur adipiscing elit. Etiam sollicitudin eu  ibero sit amet posuere. Aenean malesuada lacus eget bibendum euismod. Vestibulum hendrerit eros vitae dapibus finibus. Suspendisse potenti. Suspendisse imperdiet lobortis purus, eu eleifend leo imperdiet id.
                        <br /> <br />
                        amet, consectetur adipiscing elit. Etiam sollicitudin eu  ibero sit amet posuere. Aenean malesuada lacus eget bibendum euismod. Vestibulum hendrerit eros vitae dapibus finibus. Suspendisse potenti. Suspendisse imperdiet lobortis purus, eu eleifend leo imperdiet id.
                    </p>
                </div>
                <div className="w-full md:w-2/5">
                    <img src={testimg} width="100%" height="100%" alt="worker" className="object-cover rounded-3xl h-full" />
                </div>
            </div>
            <p className="text-base sm:text-lg lg:text-xl font-semibold text-gray-600 text-center mt-12">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin eu libero sit amet posuere. Aenean malesuada lacus eget bibendum euismod. Vestibulum hendrerit eros vitae dapibus finibus. Suspendisse potenti. Suspendisse imperdiet lobortis purus, eu eleifend leo imperdiet id.
            </p>
            <button className="rounded-full text-lg sm:text-xl md:text-2xl lg:text-3xl text-white px-4 sm:px-10 pt-3.5 pb-3 mx-auto block mb-12 bg-blue-400 mt-12 hover:bg-blue-500 duration-200">
                Umów się na bezpłatną prezentację
            </button>
        </AutoHeightTransition>
    )
}

export default SolutionDetail