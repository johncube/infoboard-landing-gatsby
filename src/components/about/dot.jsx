import React from 'react'

import { useState } from "react"

const Dot = ({ delay }) => {
    const [animation] = useState(false)

    return (
        <div className="mt-4 md:mt-0 mx-auto"
            data-aos="fade-up"
            data-aos-anchor-placement="center-bottom"
            data-aos-delay={delay}
            data-aos-offset="100"
        >
            <div className={`rounded-full ${animation ? 'bg-blue-400 w-1 h-2 sm:w-3 sm:h-3 lg:w-4 lg:h-4 ' : 'bg-blue-300 w-1 h-1 sm:w-3 sm:h-3 lg:w-3 lg:h-3 '} duration-500`}></div>
        </div>
    )
}

export default Dot