import React, { useState } from 'react'
import Solution from './solution/solution'

import trucks from '../../images/trucks_solution.png'
import hands from '../../images/hands_solution.png'
import health from '../../images/healthcare_solutions.png'
import restaurants from '../../images/restaurants_solutions.png'
import production from '../../images/production_solution.png'
import { StaticImage } from 'gatsby-plugin-image'

const Solutions = () => {
    const [open, setOpen] = useState(-1)
    return (
        <div className="relative">
            <div className="hidden xl:block w-44 h-44 absolute -right-24 -top-10">
                <StaticImage src="../../images/semicircle.png" alt="dots" />
            </div>
            <div className="hidden xl:block w-40 h-40 absolute -right-20 -top-40">
                <StaticImage src="../../images/dots.png" alt="dots" />
            </div>
            <div className="container mx-auto mt-36 mb-12 px-2 xl:px-32">
                <Solution setOpen={setOpen} open={open} id={0} title={'Zakłady Produkcyjne'} bg={production} />
                <Solution setOpen={setOpen} open={open} id={1} title={'Logistyka'} bg={trucks} />
                <Solution setOpen={setOpen} open={open} id={2} title={'Sprzedaż & Handel'} bg={hands} />
                <Solution setOpen={setOpen} open={open} id={3} title={'Restauracje'} bg={restaurants} />
                <Solution setOpen={setOpen} open={open} id={4} title={'Służba Zdrowia'} bg={health} />
            </div>
        </div>

    )
}

export default Solutions