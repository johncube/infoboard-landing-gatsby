import React from 'react'

// import hand_image from '../../images / hand.png'
// import megaphone_image from '../../images/megaphone.png'
// import hands_image from '../../images/hands.png'
import semicircle from '../../images/semicircle.png'
import Dot from './dot'
import Circle from './circle'

const About = ({ slide, changeSlide, t }) => {
    return (
        <div className="w-full relative">
            <div className="mt-20 2xl:mt-44 container mx-auto px-0 2xl:px-36 relative">
                <h1 className="pl-10 sm:pl-0 text-3xl sm:text-4xl lg:text-5xl mb-16 font-bold text-gray-700">{t.header}</h1>
                <div className="grid grid-cols-8 gap-1 md:h-56 lg:h-64 px-10 sm:px-0">
                    <Circle delay={0} img='information' text={t.text1} active={slide === 1} changeSlide={changeSlide} slide={1} imgclass="ml-2" />
                    <div className="flex flex-row justify-evenly items-center mb-12 sm:mb-6 md:h-56 lg:h-64">
                        <Dot delay={50} />
                        <Dot delay={100} />
                        <Dot delay={150} />
                    </div>
                    <Circle delay={200} img='communication' text={t.text2} active={slide === 2} changeSlide={changeSlide} slide={2} imgclass="ml-2" />
                    <div className="flex flex-row justify-evenly items-center mb-12 sm:mb-6 md:h-56 lg:h-64">
                        <Dot delay={250} />
                        <Dot delay={300} />
                        <Dot delay={350} />
                    </div>
                    <Circle delay={400} img='engagement' text={t.text3} active={slide === 3} changeSlide={changeSlide} slide={3} />
                </div>
            </div>

            <img src={semicircle} className="w-32 2xl:w-56 absolute -left-24 2xl:-left-28 top-4" alt="semicircle" />
        </div>
    )
}

export default About