import React from 'react'

import communication_image2 from '../../images/communication_new.png'

const Communication = ({ slide, t }) => {

    return (
        <div className={`absolute top-0 left-0 duration-1000 ${slide !== 2 && ' invisible opacity-0'}`}>
            <div className="sm:px-10">
                <div className="grid grid-cols-1 lg:grid-cols-2 h-auto lg:h-96 ">
                    <div className="pl-10 sm:pl-0 2xl:pl-28 h-full flex flex-col  justify-start items-start pt-10 pr-10 2xl:pr-0 ">
                        <h1 className="text-4xl text-blue-400 font-bold">{t.header}</h1>
                        <p className="mt-3 mb-4 font-bold">{t.text1}</p>
                        <p className="w-full lg:w-3/4">{t.text2}
                        </p>
                        <a href="#contact" className="text-lg sm:text-xl font-bold w-full sm:w-auto text-white bg-blue-400 px-12 py-3 rounded-full mt-8 hover:bg-blue-500 duration-150 hover:shadow-xl focus:outline-none active:outline-none cursor-pointer text-center">{t.button}</a>
                    </div>
                    <div className="relative h-80 w-full mx-4 pr-12 mt-8 md:mt-16 lg:mt-0">
                        <img src={communication_image2} className="block mx-auto lg:mr-auto max-h-full max-w-full" alt="communication" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Communication