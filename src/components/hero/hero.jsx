import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

import Scroll from '../scroll/scroll'
import { I18nextContext } from 'gatsby-plugin-react-i18next'
import { graphql, useStaticQuery } from 'gatsby'

import dots from '../../images/dots_white.png'
import semicircle from '../../images/semicircle.png'
import styled from 'styled-components'

// import pdf from '../../files/komunikuje.pl.pdf'

const Hero = ({ t }) => {
    const data = useStaticQuery(query)
    const context = React.useContext(I18nextContext)

    return (
        <div className="h-auto xl:h-screen bg-gray-700 xl:bg-white pb-10 relative pt-10 sm:pt-0">
            {context.language === 'en' && <StaticImage loading="eager" src="../../images/top_en.png" alt="Dots" className="hero-image" imgClassName="hidden sm:block w-full relative -top-6" />}
            {context.language === 'pl' && <StaticImage loading="eager" src="../../images/top.png" alt="Dots" className="hero-image" imgClassName="hidden sm:block w-full relative -top-6" />}
            {/* {context.language === 'pl' && <StaticImage src="../../images/top.png" alt="Dots" className="hidden sm:block w-full relative -top-6" />} */}
            {/* <img src={(i18n.language === 'pl' ? require('../../images/top.png').default : require('../../images/top_en.png').default)} className="hidden sm:block w-full relative -top-6" alt="" /> */}
            <div className="xl:absolute top-1/3 2xl:top-1/2 left-24 mt-0 sm:-mt-20 2xl:-mt-12 pl-10 pr-10 xl:pr-0 xl:pl-0 pt-24 sm:pt-0 xl:pt-0 w-full xl:w-1/2 2xl:w-2/5">
                <h1 className="text-white font-bold text-3xl 2xl:text-4xl">{t.header}</h1>
                <p className="text-white font-light text-lg 2xl:text-xl my-8 tracking-wider ">{t.text1}</p>
                <p className="text-white font-bold text-xl 2xl:text-xl mb-8">{t.text2}</p>
                <div className="flex items-start flex-col">
                    <a href="#contact" className="text-xl font-bold text-blue-500 bg-white px-12 py-3 rounded-full hover:bg-gray-100 duration-150 hover:shadow-xl focus:outline-none active:outline-none block sm:inline text-center w-full sm:w-auto cursor-pointer">{t.button1}</a>
                    {context.language === 'pl' && <a href={data.exampleImage.publicURL} target="_blank" rel="noreferrer" className="text-xl font-bold text-white bg-blue-400 px-12 py-3 rounded-full mt-4 hover:bg-blue-500 duration-150 hover:shadow-xl focus:outline-none active:outline-none block sm:inline w-full sm:w-auto text-center">{t.button2}</a>}
                    {context.language === 'en' && <a href={data.englishFile.publicURL} target="_blank" rel="noreferrer" className="text-xl font-bold text-white bg-blue-400 px-12 py-3 rounded-full mt-4 hover:bg-blue-500 duration-150 hover:shadow-xl focus:outline-none active:outline-none block sm:inline w-full sm:w-auto text-center">{t.button2}</a>}
                </div>
            </div>

            <Scroll />
            <img src={dots} alt="dots" className="w-40 absolute z-0 -right-6 -top-4 hidden xl:block" />
            <img src={semicircle} alt="dots" className="w-40 absolute -right-20 top-28 hidden xl:block" />
            {/* <StaticImage src='../../images/dots_white.png' alt="Dots" className="w-40 absolute -right-6 -top-4 hidden xl:block" /> */}
            {/* <StaticImage src='../../images/semicircle.png' alt="Dots" className="w-40 absolute -right-20 top-28 hidden xl:block" /> */}
        </div>
    )
}

export default Hero

export const query = graphql`
    query HeaderQuery {
        exampleImage: file( extension: {eq: "pdf"}, name: {eq: "komunikuje"}) {
            publicURL
        },
        englishFile: file( extension: {eq: "pdf"}, name: {eq: "komunikuje_en"}) {
            publicURL
        }
    }
`