import { StaticImage } from 'gatsby-plugin-image'
import React, { useState } from 'react'
import dots from '../../images/dots.png'
import Privacy from './privacy/privacy'

import image from '../../images/contact.png'

const Contact = ({ t, privacy }) => {

    const [showPrivacy, togglePrivacy] = useState(false)

    const [agree, setAgree] = useState(false)
    const [name, setName] = useState("")
    const [nameError, setNameError] = useState(false)

    const [emailAddress, setEmail] = useState("")
    const [emailError, setEmailError] = useState(false)

    const [phoneNumber, setPhone] = useState("")
    const [phoneError, setPhoneError] = useState(false)

    const [company, setCompany] = useState("")
    const [companyError, setCompanyError] = useState(false)

    const [text, setText] = useState("")

    const [emailSent, setEmailSent] = useState(false)

    const onSubmit = async (e) => {
        e.preventDefault()
        if (!name) setNameError(true)
        else setNameError(false)
        if (!emailAddress) setEmailError(true)
        else setEmailError(false)
        if (!phoneNumber) setPhoneError(true)
        else setPhoneError(false)
        if (!company) setCompanyError(true)
        else setCompanyError(false)
        // if(!agree) setAgree(true)
        if (!name || !emailAddress || !phoneNumber || !company || !agree) return;

        var xhr = new XMLHttpRequest();

        xhr.addEventListener('load', () => {
            console.log(xhr.responseText)
            if (xhr.responseText === 200) {
                setEmailSent(true)
            }
        });
        // open the request with the verb and the url
        xhr.open('GET', 'https://komunikuje.pl/mailer.php?sendto=' + emailAddress +
            '&name=' + name +
            '&phone=' + phoneNumber + '&company=' + company + '&message=' + text);
        xhr.send();
    }

    return (
        <React.Fragment>
            <div className="py-12 mt-16 bg-gradient-to-t bg-gray-200 text-center relative" id="contact">
                {/* <StaticImage src="../../images/contact.png" width="100%" height="100%" alt="contact" imgClassName="object-cover" className="absolute left-0 top-0 z-0" /> */}
                <img src={image} alt="contact" className="hidden lg:block absolute left-0 top-0 w-full h-full object-cover" />
                <img src={dots} className="hidden 2xl:block absolute -top-20 -right-12 w-40 h-40" alt="dots" />
                <div className="lg:container mx-auto px-0 md:px-2 2xl:px-36 relative z-10">
                    <div className="grid grid-cols-1 md:grid-cols-3 2xl:grid-cols-2">
                        <div className="text-left xl:pl-10">
                            <h1 className="text-3xl lg:text-5xl pl-4 font-bold text-gray-700">
                                {t.header}
                            </h1>
                            <p className="text-lg lg:text-xl 2xl:text-3xl mt-4 pl-4 mb-8 lg:mb-20 font-semibold text-gray-700">{t.text1}</p>
                        </div>
                        <div className="px-6 lg:px-0 mt-8 md:mt-0 col-span-2 2xl:col-span-1">
                            <div className="bg-white rounded-3xl py-8 px-6 w-full sm:w-3/4 lg:w-2/3 mx-auto border-2 sm:border-4 lg:border-8 border-blue-400">
                                <h1 className="text-3xl font-bold mb-4">{t.form.header}</h1>
                                {/* <p className="tracking-wide">
                                    {t.form.text}
                                </p> */}
                                <form className="px-2" onSubmit={e => onSubmit(e)}>
                                    <input
                                        type="text"
                                        placeholder={t.form.input1}
                                        className={`w-full py-1 border-b-2 ${nameError ? 'border-red-300' : 'border-blue-300'} placeholder-gray-700 mt-6  focus:outline-none focus:border-blue-500 `}
                                        value={name}
                                        onChange={e => setName(e.target.value)}
                                    />
                                    <input
                                        type="email"
                                        placeholder={t.form.input2}
                                        className={`w-full py-1 border-b-2 ${emailError ? 'border-red-300' : 'border-blue-300'} placeholder-gray-700 mt-8  focus:outline-none focus:border-blue-500`} value={emailAddress}
                                        onChange={e => setEmail(e.target.value)}
                                    />
                                    <input
                                        type="text"
                                        placeholder={t.form.input3}
                                        className={`w-full py-1 border-b-2 ${phoneError ? 'border-red-300' : 'border-blue-300'} placeholder-gray-700 mt-8  focus:outline-none focus:border-blue-500`} value={phoneNumber}
                                        onChange={e => setPhone(e.target.value)}
                                    />
                                    <input
                                        type="text"
                                        placeholder={t.form.input4}
                                        className={`w-full py-1 border-b-2 ${companyError ? 'border-red-300' : 'border-blue-300'} placeholder-gray-700 mt-8  focus:outline-none focus:border-blue-500`}
                                        value={company}
                                        onChange={e => setCompany(e.target.value)} />
                                    <div className="flex items-center justify-center mt-4">
                                        <div>
                                            <input id="privacy" type="checkbox" className={`w-5 h-5 cursor-pointer border-gray-500 appearance-none border-2 checked:bg-blue-400 checked:border-transparent`} onClick={() => setAgree(!agree)}></input>
                                        </div>
                                        <label htmlFor="privacy" className="block text-left text-gray-500 ml-4 text-md">
                                            {t.form.agreement}
                                            <button className="text-blue-500 cursor-pointer contents" onClick={() => togglePrivacy(true)}>{t.form['agreement-clickable']}</button>
                                        </label>
                                    </div>
                                    <input
                                        type="email"
                                        placeholder={t.form.input5}
                                        className={`w-full py-1 border-b-2 ${emailError ? 'border-red-300' : 'border-blue-300'} placeholder-gray-700 mt-4  focus:outline-none focus:border-blue-500`} value={text}
                                        onChange={e => setText(e.target.value)}
                                    />
                                    <div className="px-6 mt-4">
                                        <button className="text-2xl 2xl:text-3xl text-white bg-blue-400 hover:bg-blue-500 duration-150 hover:shadow-xl w-full py-2 rounded-full mt-4  focus:outline-none">{t.form.button}</button>
                                        {emailSent && <p className="pt-2 text-blue-300 font-medium">Email został wysłany</p>}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {showPrivacy && <Privacy togglePrivacy={togglePrivacy} t={privacy} />}
        </React.Fragment>
    )
}

export default Contact