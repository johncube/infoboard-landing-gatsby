import * as React from "react"
import Seo from '../components/SEO/seo'
import Footer from "../components/footer/footer"
import Navbar from "../components/navbar/navbar"
import PriceTable from "../components/price_table/price_table"
import '../styles/index.sass'
import { I18nextContext, useTranslation } from "gatsby-plugin-react-i18next"
import { graphql } from 'gatsby'

const PricePage = () => {
  const { t } = useTranslation('translation')
  const context = React.useContext(I18nextContext)
  return (
    <main >
      <Seo title={t('title')} lang={context.language} />
      <Navbar />
      <PriceTable t={t('pricelist', { returnObjects: true })} />
      <Footer />
    </main>
  )
}

export default PricePage

export const query = graphql`
  query($language: String!) {
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`;