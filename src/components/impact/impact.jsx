import React from 'react'
import Item from './item/item'

import admin_icon from '../../images/admin_icon.png'
import costs_icon from '../../images/costs_icon.png'
import env_icon from '../../images/env_icon.png'
import engagement_icon from '../../images/engagement_icon.png'

const Impact = () => {
    return (
        <div className="lg:container mx-auto px-0 md:px-2 2xl:px-44 mt-32 mb-32">
            <h1 className="text-3xl lg:text-5xl pl-4 font-bold text-gray-700">
                Przekonaj się, jak nasza aplikacja wpłynie na Twoją organizację
            </h1>
            <div className="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-2 mt-16 px-4">
                <Item img={admin_icon} title={"Administracja"} text={"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, ipsa voluptatum? Asperiores quam aliquam recusandae veritatis ullam possimus"} />
                <Item img={costs_icon} title={"Koszty"} text={"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, ipsa voluptatum? Asperiores quam aliquam recusandae veritatis ullam possimus"} />
                <Item img={env_icon} title={"Środowisko"} text={"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, ipsa voluptatum? Asperiores quam aliquam recusandae veritatis ullam possimus"} />
                <Item img={engagement_icon} title={"Zaangażowanie"} text={"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, ipsa voluptatum? Asperiores quam aliquam recusandae veritatis ullam possimus"} />
            </div>
        </div>
    )
}

export default Impact