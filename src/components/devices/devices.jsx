// import { StaticImage } from 'gatsby-plugin-image'
import { graphql, useStaticQuery } from 'gatsby'
import { I18nextContext } from 'gatsby-plugin-react-i18next'
import React from 'react'

import devices_image from '../../images/devices.png'
import devices_image_en from '../../images/devices_en.png'

// import pdf from '../../files/komunikuje.pl.pdf'

const Devices = ({ refe, t }) => {
    const ulotka = useStaticQuery(queryPL)
    // const ulotkaEN = useStaticQuery(queryEN)

    const context = React.useContext(I18nextContext)
    return (
        <div className="py-12 devices-background">
            <div className="container mx-auto px-0 2xl:px-36">
                <div className="text-center mb-12">
                    <h1 className="inline-block text-white font-bold text-2xl md:text-4xl tracking-wider"
                        data-aos="fade-zoom-in"
                        data-aos-easing="ease-in-back"
                        data-aos-offset="100"
                        data-aos-duration="1000"
                    >{t.header1}, </h1>
                    <pre className="inline-block">
                        <h1 className="inline-block text-white font-bold text-2xl md:text-4xl tracking-wider"
                            data-aos="fade-zoom-in"
                            data-aos-easing="ease-in-back"
                            data-aos-offset="100"
                            data-aos-delay="200"
                            data-aos-duration="1000"
                        > {t.header2}</h1>
                    </pre>
                    <p className="text-white font-light w-3/4 sm:w-1/2 my-8 text-sm sm:text-md lg:text-lg mx-auto tracking-wider" dangerouslySetInnerHTML={{ __html: t.text }}
                        data-aos="fade-zoom-in"
                        data-aos-easing="ease-in-back"
                        data-aos-offset="100"
                        data-aos-delay="600"
                        data-aos-duration="1200"
                    ></p>
                </div>
                {/* {context.language === 'pl' && <StaticImage src={"../../images/devices.png"} alt="One app, multiple devices" className="w-3/4 h-full block mx-auto sm:pr-10 md:pr-32" />}
                {context.language === 'en' && <StaticImage src={"../../images/devices_en.png"} alt="One app, multiple devices" className="w-3/4 h-full block mx-auto sm:pr-10 md:pr-32" />} */}
                <img src={(context.language === 'pl' ? devices_image : devices_image_en)} className="w-3/4 block mx-auto sm:pr-10 md:pr-32" alt="Devices" />
                {/* <p className="text-white text-center font-light w-3/4 sm:w-1/2 text-sm sm:text-md lg:text-lg my-8 mx-auto tracking-wider" dangerouslySetInnerHTML={{ __html: t('devices.text2') }}></p> */}
                <div className="grid grid-cols-1 md:grid-cols-2 gap-2 md:gap-0 mt-8 px-10 md:px-12 xl:px-32">
                    <div className="flex justify-center mt-4 2xl:mt-0">
                        <button onClick={() => window.scrollTo({ top: refe.current.getBoundingClientRect().top + window.pageYOffset - 100, behavior: 'smooth' })} className="text-md sm:text-xl w-full md:w-auto font-bold text-white bg-blue-400 px-8 sm:px-20 py-3 rounded-full hover:bg-blue-500 duration-150 hover:shadow-xl focus:outline-none active:outline-none text-center">{t.button1}</button>
                    </div>
                    <div className="flex justify-center mt-4 2xl:mt-0">
                        <a href={context.language === 'pl' ? ulotka.ulotka.publicURL : ulotka.ulotkaEn.publicURL} rel="noreferrer" target="_blank" className="text-md sm:text-xl w-full md:w-auto  font-bold text-white bg-green-400 px-8 sm:px-20 py-3 rounded-full hover:bg-green-500 duration-150 hover:shadow-xl  focus:outline-none active:outline-none text-center">{t.button2}</a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Devices


export const queryPL = graphql`
    query UlotkaQuery {
        ulotka: file(
        extension: {eq: "pdf"},
        name: {eq: "ulotka"}
        ) {
            publicURL
        },
        ulotkaEn: file(
            extension: {eq: "pdf"},
            name: {eq: "komunikuje_en"}
            ) {
                publicURL
            }
    }
`
// export const queryEN = graphql`
    
// `