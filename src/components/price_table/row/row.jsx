import React from 'react'

const Row = ({ text, option1, option2 }) => {
    return (
        <div className="grid grid-cols-4 py-2 border-b border-blue-400 text-sm md:text-base xl:text-lg" key={Math.random().toString()}>
            <div className="col-span-2 text-left">
                {text}
            </div>
            <div>
                {option1}
            </div>
            <div>
                {option2}
            </div>
        </div>
    )
}

export default Row