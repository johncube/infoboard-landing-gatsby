import React from 'react'

const Prices = ({t}) => {
    return (
        <div className="container mx-auto px-0 sm:px-10 xl:px-36 my-12">
            <h1 className="text-3xl sm:text-4xl lg:text-5xl pr-4 sm:px-0 pl-4 2xl:pl-12 mb-16 font-bold text-gray-700">
                {t.header}
            </h1>
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 xl:gap-0">
                <div className="flex justify-center">
                    <div className="rounded-3xl bg-blue-400 px-8 py-12 w-4/5 sm:w-full lg:w-3/4 xl:w-4/5 2xl:w-2/3 flex flex-col items-center"
                        data-aos="fade-up"
                        data-aos-anchor-placement="center-bottom"
                        data-aos-offset="200"
                    >
                        <h1 className="text-5xl text-white font-bold text-center">
                            {t.price1} {t.currency}  <span className="text-2xl">/ {t.month}</span>
                        </h1>
                        <p className="text-md text-white font-light text-center my-6 h-12 flex items-center justify-center">{t.text1}</p>
                        <a href="#contact" className="bg-white hover:bg-gray-100 duration-150 hover:shadow-xl text-blue-400 text-xl 2xl:text-3xl font-bold rounded-full px-8 py-3 tracking-wide  focus:outline-none cursor-pointer">{t.button}</a>
                    </div>
                </div>
                <div className="flex justify-center mt-14 sm:mt-0">
                    <div className="rounded-3xl bg-blue-400 px-8 py-12 w-4/5 sm:w-full lg:w-3/4 xl:w-4/5 2xl:w-2/3 flex flex-col items-center relative"
                        data-aos="fade-up"
                        data-aos-anchor-placement="center-bottom"
                        data-aos-offset="200"
                    >
                        <h1 className="text-5xl text-white font-bold text-center">
                            {t.price2} {t.currency}  <span className="text-2xl">/ {t.month}</span>
                        </h1>
                        <p className="text-md text-white font-light text-center my-6 h-12  flex items-center justify-center" dangerouslySetInnerHTML={{ __html: t.text2 }}></p>
                        <a href="#contact" className="bg-white hover:bg-gray-100 duration-150 hover:shadow-xl text-blue-400 text-xl 2xl:text-3xl font-bold rounded-full px-8 py-3 tracking-wide  focus:outline-none cursor-pointer">{t.button}</a>

                        <div className="absolute -right-4 -top-4 w-72 h-52 overflow-hidden">
                            <div className="bg-red-500 w-60 h-12 px-12 transform rotate-45 text-center text-white relative top-8 left-28 font-semibold ribbon">
                                <div className="w-60 h-12 relative right-12 bg-red-500 px-12 ">
                                    {t.ribbon}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p className="text-center mt-12 text-gray-500 font-light tracking-wider px-8 md:px-0">{t.text3}</p>
        </div>
    )
}

export default Prices