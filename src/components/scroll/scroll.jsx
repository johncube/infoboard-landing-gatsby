import React from 'react'

const Scroll = () => {
    return (
        <div className="scroll-downs">
            <div className="mousey">
                <div className="scroller"></div>
            </div>
        </div>
    )
}

export default Scroll