import React from 'react'
import styled from 'styled-components'
import SolutionDetail from '../solution_detail/solution_detail'

const Solution = ({ setOpen, open, id, title, bg }) => {
    return (
        <React.Fragment>
            <SolutionContainer className={`relative w-full py-8 md:py-14 lg:py-20 px-4 md:px-14 xl:py-24 ${id % 2 === 0 ? 'bg-blue-400' : 'bg-gray-600 text-right'} mb-4 cursor-pointer`} onClick={() => setOpen(id === open ? -1 : id)}>
                <ImageContainer odd={id % 2 === 0}>
                    {id % 2 !== 0 && <img src={bg} width="100%" height={'100%'} alt="background" className="object-cover -mt-px h-full" />}
                    {id % 2 === 0 && <img src={bg} width="100%" height={'50px'} alt="background" className="-mt-px h-full" />}
                </ImageContainer>
                <h1 className="text-2xl lg:text-3xl xl:text-4xl 2xl:text-5xl font-bold text-white z-10 relative">{title}</h1>
            </SolutionContainer>
            <SolutionDetail open={id === open} />
        </React.Fragment>
    )
}

export default Solution

const SolutionContainer = styled.div`
    border-radius: 2.5rem;
    overflow: hidden;

    @media (max-width: 768px){
        border-radius: 1.2rem;
    }
`

const ImageContainer = styled.div`
    height: 102%;
    position: absolute;
    ${props => props.odd ? 'right: 0;' : 'left: 0;'}
    top: 0;
`