import { graphql } from 'gatsby';
import { StaticImage } from 'gatsby-plugin-image';
// import { GatsbyImage } from 'gatsby-plugin-image/dist/src/components/gatsby-image.browser';
import { Link } from 'gatsby-plugin-react-i18next';
import React, { useEffect, useState } from 'react'
import Markdown from 'react-markdown'
import styled from 'styled-components';
import Footer from '../../footer/footer';
import Navbar from '../../navbar/navbar';
import Seo from '../../SEO/seo'


export const query = graphql`
  query($slug: String!) {
    
    strapiArticles(slug:{eq:$slug}){
        title
        teaser
        text
        thumbnail{
            url
        }
        files{
            url
        }
    }
  }
`;

const Detail = ({ data }) => {
    const [article, setArticle] = useState({})

    // const articleData = data

    useEffect(() => {
        if (data && 'strapiArticles' in data) {
            setArticle(data.strapiArticles)
        }
    }, [data])

    return (
        <div>
            {article.title && <div>
                <Seo title={article.title} />
                <div className="hidden xl:block w-56 h-56 absolute -right-28 top-1/3">
                    <StaticImage loading="eager" src="../../../images/semicircle.png" alt="dots" />
                </div>
                <div className="hidden xl:block w-56 h-56 absolute -left-24 top-3/4">
                    <StaticImage src="../../../images/dots.png" alt="dots" />
                </div>
                <Navbar />
                <div className="mt-44 pb-16">
                    <div className="container mx-auto px-4 sm:px-1 xl:px-32">
                        <p className="mb-12">
                            <Link to="/">
                                <StaticImage src="../../../images/home.png" alt="Home" className="w-6 mr-1" />
                            </Link> | <Link to="/blog">Blog</Link> | <span className="font-bold">{article.title}</span>
                        </p>
                        <div className="flex flex-col sm:flex-row">
                            <img className="w-full sm:w-64 lg:w-96 h-64 sm:h-56 lg:h-80 object-cover rounded-xl" src={article.thumbnail.url} alt="pracownik" />
                            <div className="sm:pl-10 flex flex-col justify-around mt-6 sm:mt-0">
                                <p className="text-lg sm:text-xl xl:text-2xl text-blue-400 font-bold">
                                    {article.title}
                                </p>
                                <p className="text-base sm:text-lg font-bold text-gray-600 mt-3 sm:mt-0">
                                    {article.teaser}
                                </p>
                            </div>
                        </div>
                        <div className="flex flex-col sm:flex-row mt-24">
                            <MarkdownContainer className="sm:pr-20 mb-10 sm:mb-0">
                                <Markdown>
                                    {article.text}
                                </Markdown>
                            </MarkdownContainer>
                            <div className="flex-shrink-0">
                                {article.files.map(f => {
                                    // return <GatsbyImage src={'http://localhost:1337' + f.url} image={'http://localhost:1337/uploads/polandrock_29_07_2021_11_07_1_f525ed024a.png'} alt="image" />
                                    return <img className="w-full sm:w-64 lg:w-96 h-64 sm:h-56 lg:h-80 object-cover rounded-xl mt-4" src={f.url} alt="file" />
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>}
        </div>
    )
}

export default Detail

const MarkdownContainer = styled.div`
    & p{
        font-size: 18px;
    }
    & h1{
        font-size: 32px;
        font-weight: 700;
        margin: 10px 0;
    }
    & h2{
        font-size: 26px;
        font-weight: 700;
        margin: 10px 0;
    }
`