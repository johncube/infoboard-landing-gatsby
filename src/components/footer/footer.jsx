import React from 'react'

import logo_white from '../../images/logo_white.png'

const Footer = () => {
    return (
        <div className="py-12 bg-gradient-to-t bg-gray-700 text-center relative">
            <div className="w-20 h-20 bg-blue-400 absolute -left-6 -top-10"></div>
            <img src={logo_white} width="100%" height="100%" className="w-20 mx-auto" alt="logo" />
            <p className="w-96 text-white mx-auto mt-8 font-light text-md">
                <span className="font-bold">© JohnCube 2021</span>, All rights reserved <br />
                JohnCube Sp. z o.o., Św. Józefa 13, 44-200 Rybnik
            </p>
        </div>
    )
}

export default Footer