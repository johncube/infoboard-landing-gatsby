import React, { useState } from "react"

const AccordionItem = ({ heading, text, last, table, t }) => {
    const [isOpen, toggle] = useState(false)

    return (
        <div className={`w-full bg-white ${!last && 'border-b-3'} border-blue-400`}>
            <button className={`w-full text-left text:xl sm:text-2xl ${isOpen ? 'text-white bg-blue-400 font-bold' : 'text-gray-700 bg-white'} font-medium px-5 py-3 duration-200 hover:bg-blue-400 hover:text-white cursor-pointer`} onClick={() => toggle(!isOpen)}>{heading}</button>
            {!table && <div className={`px-5 ${isOpen ? 'max-h-110 sm:max-h-100 md:max-h-80 xl:max-h-72 pt-3 pb-5' : 'max-h-0'}  overflow-hidden transition-all duration-200 text-sm sm:text-base`}>
                {text}
            </div>}
            {table &&
                <div className={`px-5 ${isOpen ? 'max-h-110 sm:max-h-100 pt-3 pb-5' : 'max-h-0'}  overflow-hidden transition-all duration-200 grid grid-cols-1 sm:grid-cols-3 gap-4  text-sm sm:text-base`}>
                    <div className="sm:border-r border-gray-800 px-2">
                        <p className="font-medium">{t.colname1}</p>
                        {t.col1.map(i => <span key={i} className="block">
                            - {i}
                        </span>)}
                    </div>
                    <div className="sm:border-r mt-3 sm:mt-0 border-gray-800 px-2">
                        <p className="font-medium">{t.colname2}</p>
                        {t.col2.map(i => <span key={i} className="block">
                            - {i}
                        </span>)}
                    </div>
                    <div className="px-2  mt-3 sm:mt-0 ">
                        <p className="font-medium">{t.colname3}</p>
                        {t.col3.map(i => <span key={i} className="block">
                            - {i}
                        </span>)}
                    </div>
                </div>
            }
        </div>
    )
}

export default AccordionItem