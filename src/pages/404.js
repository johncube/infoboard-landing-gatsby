import * as React from "react"
import { Link } from "gatsby"
import Navbar from "../components/navbar/navbar"
import Footer from "../components/footer/footer"
import styled from "styled-components"
import Seo from '../components/SEO/seo'
import { I18nextContext } from "gatsby-plugin-react-i18next"

// markup
const NotFoundPage = () => {
  const context = React.useContext(I18nextContext)

  return (
    <main className="min-h-screen sm:h-screen flex flex-col justify-between w-full">
      <Navbar />
      <Seo title={"Page not found"} lang={context.language} />
      <div className="flex-1 flex flex-col items-center justify-center pt-44">
        <Number className="font-black text-center text-blue-400">404</Number>
        <h2 className="text-lg sm:text-2xl lg:text-3xl font-bold mt-6 text-center">Oops... nie udało się znaleźć tego czego szukasz.</h2>
        <Link to="/" className="text-white bg-blue-400 hover:bg-blue-500 duration-200 px-10 py-3 rounded-md font-black uppercase mt-6">Strona główna</Link>
      </div>
      <Footer />
    </main>
  )
}

export default NotFoundPage


const Number = styled.h1`
  font-size: 18vw;
  line-height: 18vw;
  text-shadow: 8px 0 rgb(0, 51, 66);

  @media (max-width: 1024px){
    font-size: 23vw;
  line-height: 23vw;
  }

  @media (max-width: 640px){
    font-size: 33vw;
  line-height: 33vw;
  }
`