import * as React from "react"
import { I18nextContext, useTranslation, } from 'gatsby-plugin-react-i18next';
import { graphql } from 'gatsby'
import Footer from "../components/footer/footer";
import Nav from "../components/navbar/navbar";

import '../styles/index.sass'
import Seo from "../components/SEO/seo";
import Detail from "../components/blog/detail/detail";

// markup
const BlogPage = () => {
  const { t } = useTranslation('translation');
  const context = React.useContext(I18nextContext)
  return (
    <main >
      <Seo title={t('title')} lang={context.language} />
      {/* {loading && <Loader />} */}
      <Nav />
      <Detail />
      <Footer />
    </main>
  )
}

export default BlogPage


export const query = graphql`
  query($language: String!) {
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`;