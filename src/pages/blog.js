import * as React from "react"
import { graphql, useStaticQuery } from 'gatsby'
import Footer from "../components/footer/footer";
import Nav from "../components/navbar/navbar";

import '../styles/index.sass'
import Seo from "../components/SEO/seo";
import PostList from "../components/blog/post_list/post_list";
import { I18nextContext } from "gatsby-plugin-react-i18next";

// markup
const BlogPage = () => {
  const [posts, setPosts] = React.useState([])
  const { allStrapiArticles } = useStaticQuery(strapiquery)
  const context = React.useContext(I18nextContext)
  // const { t } = useTranslation('translation');

  React.useEffect(() => {
    // console.log(allStrapiArticles.edges)
    setPosts(allStrapiArticles.edges)
  }, [allStrapiArticles.edges])

  return (
    <main >
      <Seo title={'Blog'} lang={context.language} />
      {/* {loading && <Loader />} */}
      <Nav />
      <PostList posts={posts} />
      <Footer />
    </main>
  )
}

export default BlogPage

const strapiquery = graphql`
  {
    allStrapiArticles {
      edges {
        node {
          id
          title
          teaser
          text
          slug
          thumbnail{
            url
          }
        }
      }
    }
  }
`;