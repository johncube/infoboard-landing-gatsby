import React from 'react'

const Loader = () => {
    return (
        <div className="w-full h-screen fixed left-0 top-0 z-50 bg-white flex justify-center items-center">
            {/* <div className="w-12 h-12 bg-blue-800"></div> */}
            <div className="lds-ripple"><div></div><div></div></div>
        </div>
    )
}

export default Loader