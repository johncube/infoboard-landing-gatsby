import React from 'react'
import jc_logo from '../../../images/logo.png'

const Privacy = ({ togglePrivacy, t }) => {
    return (
        <React.Fragment>

            <div className="fixed top-1/2 left-1/2 z-50 transform -translate-x-1/2 -translate-y-1/2 bg-white w-full md:w-auto">
                <div className="px-4 flex justify-between items-center py-2">
                    <img src={jc_logo} width="100%" height="100%" className="w-12" alt="logo" />
                    <button className="text-2xl text-gray-600 cursor-pointer  px-2 py-1" onClick={() => togglePrivacy(false)}>&#10006;</button>
                </div>
                <div className="w-full relative px-4 py-2 overflow-y-scroll max-h-96  max-w-full md:max-w-2xl 2xl:max-w-xl privacy">
                    <p className="font-bold mb-4">{t.heading}</p>
                    <div dangerouslySetInnerHTML={{ __html: t.text }}></div>
                </div>
            </div>
            <div className="w-full h-screen bg-black opacity-50 fixed top-0 left-0 z-40">

            </div>
        </React.Fragment >
    )
}

export default Privacy