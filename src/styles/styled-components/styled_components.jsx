import styled from 'styled-components'

export const AutoHeightTransition = styled.div`
    max-height: ${props => props.open ? '200rem' : '0'};
    transition: max-height .6s;
    overflow: hidden;
`