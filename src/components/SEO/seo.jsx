import React from 'react'
import { Helmet } from "react-helmet"
import icon from '../../images/favicon.ico'

const Seo = ({ title, lang }) => {
    return (
        <Helmet htmlAttributes={{ lang }}>
            <meta name="description" content=".komunikuje - panel informacyjny dla Twojej firmy" />
            <meta name="keywords" content="komunikuje, komunikacja wewnętrzna, komunikacja na produkcji, zaangażowanie pracowników, digitalizacja w HR, ankieta pracownicza,onboarding pracownika, firmowy newsletter, biuletyn firmowy, motywowanie pracowników, program komputerowy, aplikacja komputerowa, tablica ogłoszeń, informacje na telewizorach" />
            <title>{title}</title>
            <link rel="shortcut icon" href={icon} type="image/x-icon" />
        </Helmet>
    )
}

export default Seo