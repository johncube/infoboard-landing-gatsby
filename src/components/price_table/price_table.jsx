import { StaticImage } from 'gatsby-plugin-image'
import React, { useState } from 'react'
import DropdownBox from './dropdown_box/dropdown_box'
import Row from './row/row'


const PriceTable = ({ t }) => {
    const [open, setOpen] = useState(-1)

    return (
        <div>
            <div className="hidden xl:block w-56 h-56 absolute -right-28 top-1/3">
                <StaticImage src="../../images/semicircle.png" alt="dots" />
            </div>
            <div className="hidden xl:block w-56 h-56 absolute -left-24 top-3/4">
                <StaticImage src="../../images/dots.png" alt="dots" />
            </div>
            <div className="container mx-auto mt-36 mb-12 xl:px-32 text-center px-2 md:px-0 relative">
                <div className="flex flex-col md:flex-row justify-between items-center mb-16">
                    <h1 className="text-blue-500 text-2xl sm:text-3xl lg:text-4xl font-bold w-full md:w-2/5 lg:w-auto lg:max-w-screen-sm md:text-left text-center">Skorzystaj z naszych atrakcyjnych planów cenowych.</h1>
                    <button className="bg-blue-400 hover:bg-blue-500 duration-200 text-white rounded-full px-10 py-2 font-bold text-sm lg:text-base mt-4 md:mt-0">Zaoszczędź 10% w abonamencie <br />
                        rocznym (suwak + kalkulator)</button>
                </div>
                {/* table view */}
                <div className="hidden sm:block">
                    <div className="w-full grid grid-cols-5">
                        <div className="col-span-2  border-b-3 border-gray-600"></div>
                        <div className="bg-gray-600 text-center py-3 text-white text-base lg:text-lg md:text-xl">Basic
                            <p className="mt-2">(licencja dla 1 lokalizacji)</p></div>
                        <div className="bg-gray-600 text-center py-3 text-white  text-base lg:text-lg md:text-xl">Advanced <p className="mt-2">(licencja dla 1 lokalizacji)</p></div>
                        <div className="bg-gray-600 text-center py-3 text-white  text-base lg:text-lg md:text-xl">Individual</div>
                        <div className="col-span-2 text-left mt-8 mb-8 text-xl xl:text-2xl font-bold text-gray-600">{t.heading}</div>
                        <div className=" mt-8 mb-8 text-xl xl:text-2xl text-gray-600 font-bold">390zł*</div>
                        <div className=" mt-8 mb-8 text-xl xl:text-2xl text-gray-600 font-bold">690zł*</div>
                        <div className=" mt-8 mb-8 "></div>
                    </div>
                    <div className="w-full grid grid-cols-5 text-lg">
                        <div className="col-span-4 border-t-2 border-blue-400">
                            {t.rows.map(r => <Row text={r.text} option1={r.option1} option2={r.option2} />)}
                        </div>
                        <div className="px-5 flex items-center justify-center text-sm md:text-base xl:text-lg">
                            {t.individual_description}
                        </div>
                    </div>
                    <div className="w-full grid grid-cols-5 mt-8">
                        <div className="col-span-2"></div>
                        <Button />
                        <Button />
                        <Button />
                    </div>
                </div>

                {/* box view */}
                <DropdownBox background="bg-gray-600" border="border-gray-500" id={0} selected={open} setOpen={setOpen} t={t} option={'option1'} text='text-gray-600' />
                <DropdownBox background="bg-blue-400" border="border-blue-500" id={1} selected={open} setOpen={setOpen} t={t} option={'option2'} text='text-blue-400' />
                <div className="w-full py-5 bg-gray-600 rounded-xl text-white block sm:hidden mt-5">
                    <div>
                        <h1 className="text-3xl font-bold">Individual</h1>
                        <p className="px-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum maiores cumque vitae ad. Autem voluptatum omnis ratione quam iusto earum</p>
                        <button className="text-2xl bg-white text-gray-600 hover:bg-gray-100 duration-200 px-8 pt-2 pb-2 rounded-full my-2">Zainteresowany</button>
                    </div>
                </div>
                <p className="text-sm sm:text-base text-right mt-3">*Podane ceny nie zawierają VAT</p>
            </div>
        </div>

    )
}

export default PriceTable

const Button = () => {
    return (
        <div className=" text-center py-3 text-white text-xl px-1 lg:px-4">
            <button className="text-white text-sm md:text-base lg:text-xl rounded-full w-full bg-blue-400 hover:bg-blue-500 duration-200 py-2">Zainteresowany</button>
        </div>
    )
}