import React from 'react'

const Item = ({ img, title, text }) => {
    return (
        <div className="bg-blue-400 w-full rounded-3xl p-4 text-center text-white pb-20 pt-7">
            <img src={img} alt="photo" className="mx-auto h-22 mb-4" />
            <h2 className="text-3xl font-bold">{title}</h2>
            <p className="text-lg mt-6">{text}</p>
        </div>
    )
}

export default Item